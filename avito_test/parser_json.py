import json
import sys

try:
    with open("file_json\Structur.json", "r", encoding='utf-8') as read_file:
        srtucture = json.load(read_file)
    with open("file_json\Draft_values.json", "r", encoding='utf-8') as read_file:
        draft_values = json.load(read_file)
except Exception as error:
    with open("file_json\error.json", "w", encoding='utf-8') as write_file:
        json.dump({"error": {"message": "Входные файлы некорректны"}}, write_file, ensure_ascii=False)
    write_file.close()
    sys.exit(error)

list_srtucture = srtucture["params"]
list_draft_values = draft_values["values"]
read_file.close()


# на вход подается value из Structure и value из draft
def seatch_title(dict_struct, draft_value):  #
    for value in dict_struct:
        if value['id'] == draft_value:
            return value['title']


def check_value(dict_draft_value, list_srtuctures):
    for dict_srtuct in list_srtuctures:
        if dict_draft_value['id'] == dict_srtuct['id']:  # ищем id
            if 'values' in dict_srtuct:  # если нашли
                dict_srtuct['value'] = seatch_title(dict_srtuct['values'], dict_draft_value['value'])  # добавляем value
                return True
            else:
                if 'value' in dict_draft_value:
                    dict_srtuct['value'] = dict_draft_value['value']

                else:
                    print("In id " + str(dict_draft_value['id']) + " no parameter values")
                return True


for dict_draft_value in list_draft_values:  # считываем все params из Structure.json
    flg_check_value = check_value(dict_draft_value, list_srtucture)
    if not flg_check_value:
        for dict_srtuct in list_srtucture:
            if 'values' in dict_srtuct:
                for dict_srtuct_values in dict_srtuct['values']:
                    if 'params' in dict_srtuct_values:
                        for dict_srtuct_values_params in dict_srtuct_values['params']:
                            if dict_draft_value['id'] == dict_srtuct_values_params['id']:
                                if 'values' in dict_srtuct_values_params:
                                    for dict_srtuct_values_params_value in dict_srtuct_values_params['values']:
                                        if dict_srtuct_values_params_value['id'] == dict_draft_value['value']:
                                            dict_srtuct_values_params['value'] = dict_srtuct_values_params_value[
                                                'title']


with open("file_json\Structure_with_values.json", "w", encoding='utf-8') as write_file:
    json.dump({'params': list_srtucture}, write_file, ensure_ascii=False)
write_file.close()
print(list_srtucture)
