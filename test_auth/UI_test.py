from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time
from random import randrange

param = open('param.txt', 'r')
param_ = param.read().split(',')
login = param_[0]  # ввести телефон
password = param_[1]  # ввести пароль
#print(password)
path_photo = param_[2]  # ввести полный путь к файлу
param.close()

driver = webdriver.Chrome('chromedriver\\chromedriver.exe')
driver.get('http://torg.mts.ru')


def element_check(driver, object_check):
    try:
        WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.XPATH, object_check)))
        return True
    except Exception:
        print("\n\tNo such element --> " + object_check)
        print("\n\tWaiting time --> 20 s.")
        return False


class test_mts(unittest.TestCase):

    def test_01(self):
        obj_to_privat_office = '//a [@href="/auth/signin"]'
        obj_phone = '//input [@id="phone"]'
        obj_password = '//input [@id="password"]'
        obj_button = '//button [@class="btn btn_red btn_big btn_login "]'
        obj_popup__switches = '//li [@class="login-popup__switches__item login-popup__switches__item_active"]'
        obj_profile = '//div [@class="profile ng-tns-c2-1 ng-star-inserted"]'

        # логинимся
        self.assertTrue(element_check(driver, obj_to_privat_office))
        self.assertIsNone(driver.find_element_by_xpath(obj_to_privat_office).click(), msg='Error: Failed to log in --> ' + obj_to_privat_office)

        # вводим телефон
        self.assertTrue(element_check(driver, obj_phone))
        self.assertIsNone(driver.find_element_by_xpath(obj_phone).send_keys(login), msg='Error: Phone input error -->' + obj_phone)

        # вводим пароль
        self.assertTrue(element_check(driver, obj_password))
        self.assertIsNone(driver.find_element_by_xpath(obj_password).send_keys(password), msg='Error: Password input Error -->' + obj_password)

        # нажимаем войти
        self.assertTrue(element_check(driver, obj_button))
        self.assertIsNone(driver.find_element_by_xpath(obj_button).click(), msg='Error: Button not found -->' + obj_button)

        # подтверждение фл
        self.assertTrue(element_check(driver, obj_popup__switches))
        self.assertIsNone(driver.find_element_by_xpath(obj_popup__switches).click(), msg='Error: -->' + obj_popup__switches)

        # проверка входа на торговую площадку мтс
        self.assertEqual(element_check(driver, obj_profile), True, msg='Error test_01: --> ' + obj_profile)

    def test_02(self):
        obj_profile = '//div [@class="profile ng-tns-c2-1 ng-star-inserted"]'
        obj_to_profile_setting = '//a [@href="/user/profile"]'
        obj_profile_firstName = '//input [@id="firstName"]'
        obj_profile_lastName = '//input [@id="lastName"]'
        obj_save = '//button [@class="button"][1]'

        # нажимаем на профиль
        self.assertTrue(element_check(driver, obj_profile))
        self.assertIsNone(driver.find_element_by_xpath(obj_profile).click(), msg='Error: Does not click --> ' + obj_profile)

        # заходим в настройки профиля
        self.assertTrue(element_check(driver, obj_to_profile_setting))
        self.assertIsNone(driver.find_element_by_xpath(obj_to_profile_setting).click(), msg='Error: Does not click --> ' + obj_to_profile_setting)

        # очищаем значение поля firstName и саписаваем своё
        self.assertTrue(element_check(driver, obj_profile_firstName))
        self.assertIsNone(driver.find_element_by_xpath(obj_profile_firstName).clear(), msg='Error: Not cleared --> ' + obj_profile_firstName)
        self.assertIsNone(driver.find_element_by_xpath(obj_profile_firstName).send_keys(str(randrange(1, 100, 1))), msg='Error: Not filled --> ' + obj_profile_firstName)

        # очищаем значение поля lastName и саписаваем своё
        self.assertTrue(element_check(driver, obj_profile_lastName))
        self.assertIsNone(driver.find_element_by_xpath(obj_profile_lastName).clear(), msg='Error: Not cleared --> ' + obj_profile_lastName)
        self.assertIsNone(driver.find_element_by_xpath(obj_profile_lastName).send_keys(str(randrange(1, 100, 1))), msg='Error: Not filled --> ' + obj_profile_lastName)

        self.assertEqual(driver.find_element_by_xpath(obj_save).click(), None, msg='Error: test_02 --> ' + obj_save)

    def test_03(self):
        obj_input_photo = '//input [@accept="image/jpeg, image/png"]'
        obj_button_seve_photo = '//button [@class="btn"][2]'

        # добавляем фото
        self.assertTrue(element_check(driver, obj_input_photo))
        self.assertIsNone(driver.find_element_by_xpath(obj_input_photo).send_keys(path_photo), msg='Error: Not input photo --> ' + obj_input_photo)

        # сохраняем фото
        self.assertTrue(element_check(driver, obj_button_seve_photo))
        self.assertEqual(driver.find_element_by_xpath(obj_button_seve_photo).click(), None, msg='Error: test_03 --> ' + obj_button_seve_photo)

    def test_04(self):
        obj_profile = '//div [@class="profile ng-tns-c2-1 ng-star-inserted"]'
        obj_exit = '//li [@class="profile__dropdown__item profile__dropdown__item_logout"]'
        obj_to_privat_office = '//a [@href="/auth/signin"]'

        # нажимаем на профиль
        self.assertTrue(element_check(driver, obj_profile))
        WebDriverWait(driver, 20).until(
            EC.element_to_be_clickable((By.XPATH, obj_profile)))
        time.sleep(1)
        self.assertIsNone(driver.find_element_by_xpath(obj_profile).click(), msg='Error: Not filled --> ' + obj_profile)

        # выходим
        self.assertTrue(element_check(driver, obj_exit))
        self.assertIsNone(driver.find_element_by_xpath(obj_exit).click(), msg='Error: Not filled --> ' + obj_exit)

        # если есть кнопка "Войти", тест успешен
        self.assertEqual(element_check(driver, obj_to_privat_office), True, msg='Error: test_04 --> ' + obj_to_privat_office)



    def test_end(self):
        driver.close()


if __name__ == '__main__':
    unittest.main()
