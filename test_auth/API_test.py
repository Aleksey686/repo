import json
import requests
import unittest

url = 'https://torg.mts.ru/api/v1/pub/auction/page'

data = json.dumps({"typeAuc": -1,
                   "sortBy": 0,
                   "sortDesc": False,
                   "page": 1,
                   "limit": 10,
                   "limitPages": 2,
                   "limitDays": 0})

headers = {'Accept': 'application/json;charset=UTF-8',
           'Content-Type': 'application/json',
           'Origin': 'https://torg.mts.ru',
           'Content-Length': '91',
           'Accept-Language': 'ru',
           'Accept-Encoding': 'br, gzip, deflate',
           'Connection': 'keep-alive'}


class test_mts(unittest.TestCase):

    def test_api_01(self):
        r = requests.post(url, data, headers=headers)
        self.assertEqual(r.status_code, 200, msg='Error: test_api_01 --> status_code = ' + str(r.status_code))


if __name__ == '__main__':
    unittest.main()
